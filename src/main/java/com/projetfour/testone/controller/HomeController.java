package com.projetfour.testone.controller;

import com.projetfour.testone.model.Coach;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HomeController {

    private Coach myCoach;

    @Autowired
    public HomeController(@Qualifier("tennisCoach") Coach myCoach) {
        System.out.println("Constructor " + myCoach.getClass().getName());
        this.myCoach = myCoach;
    }

    //injection
    @Value("${coach.name}")
    private String coachName;

    @Value("${team.name}")
    private String teamName;

    @GetMapping("/")
    public String homePage(){
        return "Hello world";
    }

    @GetMapping("/team-info")
    public String teamPage(){
        return coachName + " " + teamName;
    }

    //test one

    @GetMapping("/workout")
    public String getDailyWorkout(){
        return "Run a hard 5k!";
    }

    // test two

    @GetMapping("/work-indoor")
    public String getWorkIndoor(){
        return "Go test !";
    }

    //test coach

    @GetMapping("/coach")
    public String getMessageCoach(){
        return myCoach.getDailyWorkout();
    }
}
