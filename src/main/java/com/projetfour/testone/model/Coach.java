package com.projetfour.testone.model;

public interface Coach {

    String getDailyWorkout();
}
