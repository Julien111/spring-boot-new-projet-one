package com.projetfour.testone.model;

import org.springframework.stereotype.Component;

@Component
public class CricketCoach implements Coach {

    public CricketCoach() {
        System.out.println("In constructor " + getClass().getName());
    }

    @Override
    public String getDailyWorkout() {
        return "Practice fast for 15 min";
    }
}
