package com.projetfour.testone.model;

import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

@Component
public class FootballCoach implements Coach{

    public FootballCoach() {
        System.out.println("In constructor " + getClass().getName());
    }

    @Override
    public String getDailyWorkout() {
        return "Practice football wednesday";
    }
}
