package com.projetfour.testone.model;

import org.springframework.stereotype.Component;

@Component
public class TennisCoach implements Coach {

    public TennisCoach() {
        System.out.println("In constructor " + getClass().getName());
    }

    @Override
    public String getDailyWorkout() {
        return "Practice for tennis";
    }
}
